pragma solidity ^0.4.15;

/**
 * Creates a 50/50 charity raffle for charity.
 */
contract CharityRaffle {

  /** Triggered when a ticket is purchased */
  event NewTicket(address _owner, uint _ticketNumber);

  /** Triggered when a winner is picked */
  event Payout(address _to, uint _amount);

  /** Has this raffle ended? */
  bool public hasEnded;

  /** Person/organization setting up the raffle */
  address public organizer;

  /** Price to buy in */
  uint public ticketCost;

  /** Number of tickets sold */
  uint public ticketCount;

  /** The winner of the raffle */
  address public winner;

  /** Keeps track of who bought what */
  mapping (uint => address) ticketMap;

  /** Keeps track of who owns what */
  mapping (address => uint[]) ticketOwnerMap;
  
  /** Create a new raffle
   * @param ticketPrice - price of raffle ticket
   */
  function CharityRaffle(uint ticketPrice) {
    ticketCost = ticketPrice;
    hasEnded = false;

    // The user initiating the contract is owner.
    organizer = msg.sender;
  }

  /** Looks up how many tickets someone has bought */
  function ownerTicketCount(address addr) constant returns(uint) {
      return ticketOwnerMap[addr].length;
  }

  /** Let the world know how much is in the contract. */
  function pot() constant returns(uint) {
      // This isn't really necessary. Querying the balance of a contract seems 
      // pretty standard and a function isn't actually needed.
      return this.balance;
  }

  /** Lets any person in the WORLD play the raffle.
   * @param quantity - Number of tickets to buy.
   */
  function buyTickets(uint quantity) payable public returns (uint[] tickets) { 

    // Can't buy if raffle is over.
    require(!hasEnded);

    // Prevent shadiness...
    require(msg.sender != organizer);

    // Not sure why this fails. Maybe because of gas or stupidity...
    // require the value sent is legit
    // require(msg.value == quantity * ticketCost);
    
    // Record the purchase.
    var result = new uint[](quantity);
    for (uint i = 0; i < quantity; i++) { 
      ticketCount++;
      ticketMap[ticketCount] = msg.sender;
      ticketOwnerMap[msg.sender].push(ticketCount);

      // Broadcast events. Not really sure why but tons of
      // examples do this. I think it's hard to get info
      // out of function call otherwise.
      NewTicket(msg.sender, ticketCount);
    }

    return result;
  }

  function draw() {

    // Only the organizer is allowed to draw a ticket.
    require(msg.sender == organizer);
    require(!hasEnded);
    hasEnded = true;

    // Pick a random ticket number.
    uint result = random();
    winner = ticketMap[result];

    // Distribute the funds.
    var dist = this.balance/2;
    winner.transfer(dist);
    organizer.transfer(dist);

    // Fire events explaining what happened.
    Payout(winner, dist);
    Payout(organizer, dist);
  }

  function random() private returns(uint result) {
    // todo: Make better random generator.
    uint r = now % ticketCount;
    return r;
  }
}