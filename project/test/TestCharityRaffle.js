
var CharityRaffle = artifacts.require("../CharityRaffle.sol");
var _ = require("lodash");

contract('CharityRaffle', function (accounts) {

  /**
   * TODO:
   *  - Tests validating error checking works would be next big thing.
   *  - Write tests expressing possible edge cases.
   *  - Expand to recovery cases like refund ect.
   *  - Figure out how to detect events fired from within contract
   *    if this even possible.
   */

  var TICKET_PRICE = 10;
  var OWNER = accounts[0];

  var raffle;
  var pot;
  var organizer;
  var hasStarted;

  function printBalances() {
    var i = 0;

    console.log('raffle balance = ' + web3.eth.getBalance(raffle.address))
    console.log('account 0 balance = ' + web3.eth.getBalance(accounts[0]))
    console.log('account 1 balance = ' + web3.eth.getBalance(accounts[1]))
    console.log('account 2 balance = ' + web3.eth.getBalance(accounts[2]))
    console.log('account 3 balance = ' + web3.eth.getBalance(accounts[3]))
  }

  function createNew (done) {

    return CharityRaffle.new(TICKET_PRICE, { from: OWNER })
      .then(function (instance) {
        assert.isDefined(instance);
        raffle = instance;
        return raffle.hasEnded.call();
      }).then(function (result) {
        hasEnded = result;
        return raffle.pot();
      }).then(function (result) {
        pot = result;
        assert
        return raffle.organizer.call();
      }).then(function (result) {
        organizer = result;

        assert.isFalse(hasEnded, 0, "Raffle should not be ended.");
        assert.equal(pot, 0, "Pot should start at zero.");
        assert.equal(organizer, OWNER, "Organizer should be account 0.");
        
        done();
      }).catch(done);
  };

  describe("creation", function () {

    it("intance exists", function (done) {
      CharityRaffle.deployed().then(function (instance) {
        assert.isDefined(instance);
        done();
      }).catch(done);
    });

    it("creates", function (done) {
      createNew(done);
    });
  });

  describe("use cases", function() {

    beforeEach(function(done) {
      createNew(done);
    });

    function buy(count, account) {
      return new Promise(function(resolve){

        var val = web3.toWei(count * TICKET_PRICE, 'ether')

        console.log('trying to by ' + count + ' tickets for ' + val);

        raffle.buyTickets(count, {from: account, value: val}).then(function(result){
          resolve();
        });
      });
    }

    it('runs lotto with one person', function(done) {

      console.log("BALANCES AT START")
      printBalances();

      buy(10, accounts[1])
        .then(function(){

          console.log("BALANCES AFTER BUY")
          printBalances();

          raffle.draw({from: accounts[0]})
            .then(function(result) {
              return raffle.winner.call();
            })
            .then(function(result) {
              console.log('winner is ', result);
              assert.equal(result, accounts[1]);

              console.log('winner balance = ' + web3.eth.getBalance(result))

              console.log("BALANCES AFTER DRAW")
              printBalances();
              done();
            });
        })
        .catch(done);
    });
    
    it('run lotto with 3 persons', (done) => {
      console.log("BALANCES AT START")
      printBalances();
      let promises = [];
      for(let i = 1; i < accounts.length; ++i){
        let account = accounts[i];
        promises.push(buy(10, account));
      }
      Promise.all(promises).then(() => {
          console.log("BALANCES AFTER BUY")
          printBalances();
        }).then(() => {
          raffle.draw({from: accounts[0]})
            .then(function(result) {
              return raffle.winner.call();
            })
            .then(function(result) {
              console.log('winner is ', result);
              
              console.log('winner balance = ' + web3.eth.getBalance(result))

              console.log("BALANCES AFTER DRAW")
              printBalances();
            });
        })
        .catch(done);      
      done();      
    });
  });
});