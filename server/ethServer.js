var TestRPC = require('ethereumjs-testrpc');

var accountConfig = [
  {balance: 700000000000000000000},
  {balance: 700000000000000000000},
  {balance: 700000000000000000000},
  {balance: 700000000000000000000}
];

var server = TestRPC.server({
  accounts: accountConfig
});

server.listen(8545, function(err, blockchain) {
  console.log(err);
});