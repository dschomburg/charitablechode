### What is this repository for? ###

Learning about etherium contracts.

### How do I get set up? ###

First, run npm install in project and server folders first. Then to run tests do something like this:

~ new terminal for web server running testrcp ~
> cd server
> node ethServer.js

~ new terminal for running unit tests ~
> cd project
> truffle compile
> truffle deploy
> truffle test
